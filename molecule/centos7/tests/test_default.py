import os
import testinfra.utils.ansible_runner


testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    os.environ['MOLECULE_INVENTORY_FILE']).get_hosts('molecule_group')


def test_radiusd_running(host):
    if host.ansible("setup")["ansible_facts"]["ansible_distribution"] == "CentOS":
        service = host.service("radiusd")
        assert service.is_running
        assert service.is_enabled
    elif host.ansible("setup")["ansible_facts"]["ansible_distribution"] == "Ubuntu":
        service = host.service("freeradius")
    assert service.is_running
    assert service.is_enabled


def test_radius_clients_conf(host):
    if host.ansible("setup")["ansible_facts"]["ansible_distribution"] == "CentOS":
        assert host.file("/etc/raddb/clients.conf").content_string.strip() == """
client localhost {
    ipaddr = 127.0.0.1
    proto = *
    secret = testing123
    require_message_authenticator = no
    nas_type   = other  # localhost isn't usually a NAS...
    limit {
        max_connections = 16
        lifetime = 0
        idle_timeout = 30
    }
}

client cslab-mgmt-clients {
    ipaddr = 172.30.0.0/23
    secret = testing123
}

client cslab-routenet-clients {
    ipaddr = 172.30.255.0/27
    secret = testing123
}
    """.strip()
    elif host.ansible("setup")["ansible_facts"]["ansible_distribution"] == "Ubuntu":
        assert host.file("/etc/freeradius/3.0/clients.conf").content_string.strip() == """
client localhost {
    ipaddr = 127.0.0.1
    proto = *
    secret = testing123
    require_message_authenticator = no
    nas_type   = other  # localhost isn't usually a NAS...
    limit {
        max_connections = 16
        lifetime = 0
        idle_timeout = 30
    }
}

client cslab-mgmt-clients {
    ipaddr = 172.30.0.0/23
    secret = testing123
}

client cslab-routenet-clients {
    ipaddr = 172.30.255.0/27
    secret = testing123
}
    """.strip()
