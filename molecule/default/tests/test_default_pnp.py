import os
import testinfra.utils.ansible_runner


testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    os.environ['MOLECULE_INVENTORY_FILE']).get_hosts('molecule_group_pnp')


def test_radiusd_running(host):
    if host.ansible("setup")["ansible_facts"]["ansible_distribution"] == "CentOS":
        service = host.service("radiusd")
        assert service.is_running
        assert service.is_enabled
    elif host.ansible("setup")["ansible_facts"]["ansible_distribution"] == "Ubuntu":
        service = host.service("freeradius")
        assert service.is_running
        assert service.is_enabled


def test_radius_authorize(host):
    if host.ansible("setup")["ansible_facts"]["ansible_distribution"] == "CentOS":
        assert host.file("/etc/raddb/mods-config/files/authorize").content_string.strip() == """
0242428a00ab Cleartext-Password := "0242428a00ab",  Auth-Type := "EAP"
        Tunnel-Type = "VLAN", Tunnel-Medium-Type = "IEEE-802", Tunnel-Private-Group-id = "1901"
    """.strip()
    elif host.ansible("setup")["ansible_facts"]["ansible_distribution"] == "Ubuntu":
        assert host.file("/etc/freeradius/3.0/mods-config/files/authorize").content_string.strip() == """
0242428a00ab Cleartext-Password := "0242428a00ab",  Auth-Type := "EAP"
        Tunnel-Type = "VLAN", Tunnel-Medium-Type = "IEEE-802", Tunnel-Private-Group-id = "1901"
    """.strip()
