import os
import testinfra.utils.ansible_runner


testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    os.environ['MOLECULE_INVENTORY_FILE']).get_hosts('molecule_group_default')


def test_radius_authorize(host):
    if host.ansible("setup")["ansible_facts"]["ansible_distribution"] == "CentOS":
        assert host.file("/etc/raddb/mods-config/files/authorize").content_string.strip() == """
0242428a5dd9 Cleartext-Password := "0242428a5dd9",  Auth-Type := "EAP"
        Tunnel-Type = "VLAN", Tunnel-Medium-Type = "IEEE-802", Tunnel-Private-Group-id = "1910"
484d7ee4c7f1 Cleartext-Password := "484d7ee4c7f1",  Auth-Type := "EAP"
        Tunnel-Type = "VLAN", Tunnel-Medium-Type = "IEEE-802", Tunnel-Private-Group-id = "1614"
024242dc31bc Cleartext-Password := "024242dc31bc",  Auth-Type := "EAP"
        Tunnel-Type = "VLAN", Tunnel-Medium-Type = "IEEE-802", Tunnel-Private-Group-id = "1710"
        """.strip()
    elif host.ansible("setup")["ansible_facts"]["ansible_distribution"] == "Ubuntu":
        assert host.file("/etc/freeradius/3.0/mods-config/files/authorize").content_string.strip() == """
0242428a5dd9 Cleartext-Password := "0242428a5dd9",  Auth-Type := "EAP"
        Tunnel-Type = "VLAN", Tunnel-Medium-Type = "IEEE-802", Tunnel-Private-Group-id = "1910"
484d7ee4c7f1 Cleartext-Password := "484d7ee4c7f1",  Auth-Type := "EAP"
        Tunnel-Type = "VLAN", Tunnel-Medium-Type = "IEEE-802", Tunnel-Private-Group-id = "1614"
024242dc31bc Cleartext-Password := "024242dc31bc",  Auth-Type := "EAP"
        Tunnel-Type = "VLAN", Tunnel-Medium-Type = "IEEE-802", Tunnel-Private-Group-id = "1710"
        """.strip()
