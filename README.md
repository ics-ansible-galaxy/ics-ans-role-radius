# ics-ans-role-radius

Ansible role to install a radius server.

## Role Variables

```yaml
radius_secret: testing123
# List of network clients
# Shall be a list of dict with name and ipaddr keys
radius_network_clients: []
# radius_network_clients:
#   - name: cslab-mgmt-clients
#     ipaddr: 172.30.0.0/23
#   - name: cslab-routenet-clients
#     ipaddr: 172.30.255.0/27

# List of users
# Shall be a list of dict with mac and vlan_id keys
radius_users: []
# radius_users:
#   - mac: "02:42:42:8a:5d:d9"
#     vlan_id: 1900
```

The `radius_users` list can be populated from CSEntry inventory by using the `csentry_inventory_to_mac_vlan_id` filter plugin:
```
radius_users: "{{ hostvars | csentry_inventory_to_mac_vlan_id(domain='cslab.esss.lu.se' }}"
# OR
radius_domains:
  - tn.esss.lu.se
  - cslab.esss.lu.se
radius_users: "{{ hostvars | csentry_inventory_to_mac_vlan_id(domain=radius_domains) }}"
# OR
radius_users: "{{ hostvars | csentry_inventory_to_mac_vlan_id(only_vlan_id=1901, inherit_mac_from_main_interface=true) }}"
```

The `domain` argument is used to filter the interfaces. This argument can be a string or a list of strings.
Only interfaces part of the *cslab.esss.lu.se* domain will be returned in the first example. *tn.esss.lu.se* will be returned too in the second example (multiple domain selection).
The `only_vlan_id` argument is used to filter the interfaces. Only interfaces with vlan with id 1901 will be returned.
Setting `inherit_mac_from_main_interface` to `true` will use the mac address of the main interface to render secondary interfaces without a mac address assigned in CSEntry.



## Example Playbook

```yaml
- hosts: servers
  roles:
    - role: ics-ans-role-radius
```

## License

BSD 2-clause
