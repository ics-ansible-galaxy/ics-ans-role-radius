import sys
import json

sys.path.insert(1, "filter_plugins")
import radius


def test_filter(fake_hostvars, fake_hostvars_rendered):
    out = radius.csentry_inventory_to_mac_vlan_id(fake_hostvars)
    assert out != []
    assert json.dumps(out).strip() == fake_hostvars_rendered.strip()


def test_filter_idempotency(fake_hostvars, fake_hostvars_random_order):
    out1 = radius.csentry_inventory_to_mac_vlan_id(fake_hostvars)
    out2 = radius.csentry_inventory_to_mac_vlan_id(fake_hostvars_random_order)
    assert json.dumps(out1) == json.dumps(out2)


def test_filter_by_domain(fake_hostvars, fake_hostvars_rendered):
    out_tn = json.dumps(radius.csentry_inventory_to_mac_vlan_id(fake_hostvars, domain='tn.esss.lu.se'))
    out_cslab = json.dumps(radius.csentry_inventory_to_mac_vlan_id(fake_hostvars, domain='cslab.esss.lu.se'))
    assert '02:42:42:6f:cd:6c' in out_tn
    assert 'e8:b6:c2:93:bb:00' not in out_tn
    assert 'e8:b6:c2:93:bb:00' in out_cslab
    assert '02:42:42:6f:cd:6c' not in out_cslab
