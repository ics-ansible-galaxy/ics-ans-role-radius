import yaml
import pytest
import random


@pytest.fixture
def fake_hostvars(scope="session"):
    with open("tests/hostvars.yaml", "r") as hostvars:
        return yaml.safe_load(hostvars)


@pytest.fixture
def fake_hostvars_rendered(scope="session"):
    with open('tests/hostvars_rendered.json') as f:
        return f.read()


@pytest.fixture
def fake_hostvars_random_order(fake_hostvars):
    random_hostvars_list = list(fake_hostvars.items())
    random.shuffle(random_hostvars_list)
    return dict(random_hostvars_list)
