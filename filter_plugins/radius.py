def csentry_inventory_to_mac_vlan_id(hostvars, domain=None, only_vlan_id=None,
                                     inherit_mac_from_main_interface=False):
    """
    This function returns a list of mac/vlan_id from CSEntry inventory hostvars

    If domain is given, only MAC part of this domain will be returned
    """
    result = []
    sorted_hostvars = {k: hostvars[k] for k in sorted(hostvars)}
    for hostvar in sorted_hostvars.values():
        ztp_mac_offset = hostvar.get("ztp_mac_offset", None)
        for interface in hostvar.get("csentry_interfaces", []):
            mac = interface.get("mac", None)
            if mac is None and inherit_mac_from_main_interface:
                mac = hostvar["csentry_interfaces"][0].get("mac", None)
            if mac is not None and ztp_mac_offset is not None:
                mac = offset_mac(mac, ztp_mac_offset).lower()
            network = interface.get("network", {})
            interface_domain = network.get("domain", None)
            vlan_id = network.get("vlan_id", None)
            if only_vlan_id is not None and vlan_id != only_vlan_id:
                continue
            if domain is not None and not isinstance(domain, list) and interface_domain != domain:
                continue
            if domain is not None and isinstance(domain, list) and interface_domain not in domain:
                continue
            if mac is None or vlan_id is None:
                continue
            result.append({"mac": mac, "vlan_id": vlan_id})
    return result


def offset_mac(mac, offset):
    """
    This function modifies the mac by offset.
    """
    _mac = int(mac.replace(':', ''), 16) + offset
    if _mac < 0:
        raise Exception('MAC is negative, check offset')
    _mac = "{:012X}".format(_mac)
    _mac = ':'.join(_mac[i:i + 2] for i in range(0, 12, 2))
    return _mac


class FilterModule(object):
    def filters(self):
        return {
            "csentry_inventory_to_mac_vlan_id": csentry_inventory_to_mac_vlan_id,
            'offset_mac': offset_mac
        }
